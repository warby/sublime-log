import sublime, sublime_plugin


class logCommand(sublime_plugin.TextCommand):

    # Settings cache
    settings = None

    # Gets the named setting
    def get_setting(self, setting):
        if (self.settings == None):
            self.settings = sublime.load_settings('log.sublime-settings')

        return self.settings.get(setting)

    # Main
    def run(self, edit):

        # Map of file extensions to log commands
        ext_map = self.get_setting("commands")

        # Current file's extension
        this_ext = self.view.file_name().split(".")[-1]

        # Command to run
        try:
            command = ext_map[this_ext]
        except KeyError:
            return # Unrecognised file extension so ignore

        for region in self.view.sel():

            # If empty, just insert text and move caret
            if region.empty():
                self.view.replace(edit, region, command['command'])

                col = self.view.rowcol(self.view.sel()[0].begin())[1] + command['offset']
                row = self.view.rowcol(self.view.sel()[0].begin())[0]
                target = self.view.text_point(row, col)

            # If text selected, wrap in log using offset as split
            else:
                text = command['command'][:command['offset']]+self.view.substr(region)+command['command'][command['offset']:]
                self.view.replace(edit, region, text)

                col = self.view.rowcol(self.view.sel()[0].begin())[1] + len(text)
                row = self.view.rowcol(self.view.sel()[0].begin())[0]
                target = self.view.text_point(row, col)

            self.view.sel().clear()

            self.view.sel().add(sublime.Region(target))
